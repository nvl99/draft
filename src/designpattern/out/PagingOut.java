package designpattern.out;

import java.util.ArrayList;
import java.util.List;

public class PagingOut {

    private static final List<String> pagingData;

    static {
        pagingData = new ArrayList<>();
    }

    public void addData(List<String> data) {
        pagingData.addAll(data);
    }

    public List<String> getPagingData() {
        return pagingData;
    }

    public void printResult() {
        pagingData.forEach(System.out::println);
    }
}
