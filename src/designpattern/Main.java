package designpattern;

import designpattern.chain.Middleware;
import designpattern.in.PagingIn;
import designpattern.out.PagingOut;
import designpattern.utils.DataUtils;

public class Main {

    public static void main(String[] args) {
        PagingOut pagingOut = new PagingOut();
        PagingIn pagingIn = new PagingIn(2, 20);

        Middleware middleware = Middleware.init(DataUtils.generateTable());

        middleware.process(pagingIn, pagingOut);

//        pagingOut.printResult();
    }
}
