package designpattern.chain;

import designpattern.in.PagingIn;
import designpattern.out.PagingOut;

import java.util.List;

public abstract class Middleware {

    protected Middleware next;

    public static Middleware init(List<Middleware> chains) {
        Middleware head = chains.get(0);
        for (int i = 1; i < chains.size(); i++) {
            head.next = chains.get(i);
            head = chains.get(i);
        }
        return chains.get(0);
    }

    public void process(PagingIn pagingIn, PagingOut pagingOut) {
        if (isAvailable(pagingIn)) {
            fetchData(pagingIn, pagingOut);
        } else {
            next(pagingIn, pagingOut);
        }
    }

    protected void next(PagingIn pagingIn, PagingOut pagingOut) {
        if (this.next == null) {
            System.out.println("=======Data is not enough======");
            return;
        }
        this.next.process(pagingIn, pagingOut);
    }

    public abstract void fetchData(PagingIn pagingIn, PagingOut pagingOut);

    public abstract boolean isAvailable(PagingIn pagingIn);

}
