package designpattern.chain;

import designpattern.in.PagingIn;
import designpattern.out.PagingOut;

import java.util.List;
import java.util.stream.Collectors;

public class Table extends Middleware {

    private final List<String> data;
    private final String tableName;

    public Table(List<String> data, String tableName) {
        this.data = data;
        this.tableName = tableName;
    }

    @Override
    public void fetchData(PagingIn pagingIn, PagingOut pagingOut) {
        System.out.println("======Get data from table " + tableName);

        int skip = pagingIn.getSkip();
        int size = pagingIn.getSize();

        int limit = size - pagingOut.getPagingData().size();

        List<String> response = data.stream().skip(skip).limit(limit).collect(Collectors.toList());
        pagingOut.addData(response);
        response.forEach(System.out::println);

        if (pagingOut.getPagingData().size() < size) {
            pagingIn.setSkip(0);
            next(pagingIn, pagingOut);
        }
    }

    @Override
    public boolean isAvailable(PagingIn pagingIn) {
        int skip = pagingIn.getSkip();
        if (data.size() <= skip) {
            int nextSkip = skip - data.size();
            pagingIn.setSkip(nextSkip);
            System.out.println(tableName + " is not available");
            return false;
        }
        return true;
    }

}
