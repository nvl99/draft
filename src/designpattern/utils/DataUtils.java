package designpattern.utils;

import designpattern.chain.Middleware;
import designpattern.chain.Table;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class DataUtils {

    private final static Map<String, Integer> mapData;

    static {
        mapData = new HashMap<>();
        mapData.put("A", 20);
        mapData.put("B", 0);
        mapData.put("C", 15);
        mapData.put("D", 0);
        mapData.put("E", 25);
    }

    public static List<Middleware> generateTable() {
        return mapData.entrySet().stream().map(entry -> {
            List<String> data = fakeData(entry.getKey(), entry.getValue());
            return new Table(data, entry.getKey());
        }).collect(Collectors.toList());
    }

    public static List<String> fakeData(String preName, int number) {
        List<String> data = new ArrayList<>();
        for (int i = 1; i <= number; i++) {
            data.add(preName + i);
        }
        return data;
    }

}
