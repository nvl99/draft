package designpattern.in;

public class PagingIn {

    private int page; // min = 1
    private int size;

    // skip = (page - 1) * size
    private int skip;

    public PagingIn(int page, int size) {
        this.page = page;
        this.size = size;
        this.skip = (page - 1) * size;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getSkip() {
        return skip;
    }

    public void setSkip(int skip) {
        this.skip = skip;
    }
}
